package pl.codementors.comics;

import org.apache.commons.lang3.tuple.Pair;

import java.io.*;
import java.util.*;

/**
 * Represents comics library. Contains inner collection with comics, exposes methods for adding and removing comics.
 * Allows for changing covers of all contained comics.
 *
 * @author psysiu
 */
public class ComicsLibrary {

    /**
     * Set of comics contained in the library.
     */
    private Set<Comic> comics = new HashSet<>();

    /**
     * @return All comics in the library. The returned collection is unmodifiable so it can not be changed
     * outside the library.
     */
    public Collection<Comic> getComics() {
        return Collections.unmodifiableCollection(comics);
    }

    /**
     * Adds comic to the library. If comic is already in the library does nothing. If comic is null does nothing.
     *
     * @param comic Comic to be added to the library.
     */
    public void add(Comic comic) {
        if (comic != null) {
            if (comics.contains(comic) == false)
                comics.add(comic);
        }

    }

    /**
     * Removes comic from the library. If comics is not present in the library does nothing.
     *
     * @param comic Comic to be removed from the library.
     */
    public void remove(Comic comic) {
        if (comics.contains(comic)) {
            comics.remove(comic);
        }
    }

    /**
     * Changes covers of all comics in the library.
     *
     * @param cover Cover type for all comics in the library.
     */
    public void changeCovers(Comic.Cover cover) {
        for (Comic comic : comics) {
            comic.setCover(cover);
        }
    }

    /**
     * @return All authors of all comics in the library. Each author is present only once in the returned collection.
     * The returned collection is unmodifiable so it can not be changed outside the library.
     */
    public Collection<String> getAuthors() {
        Collection<String> autors = new TreeSet<>();
        for (Comic comic : comics) {
            if (!(autors.contains(comic.getAuthor())))
                autors.add(comic.getAuthor());

        }
        return Collections.unmodifiableCollection(autors);
    }

    /**
     * @return All series of all comics in the library. Each series is present only once in the returned collection.
     * The returned collection is unmodifiable so it can not be changed outside the library.
     */
    public Collection<String> getSeries() {

        Collection<String> series = new TreeSet<>();
        for (Comic comic : comics) {
            if (!(series.contains(comic.getSeries()))) {
                series.add(comic.getSeries());
            }
        }
        return Collections.unmodifiableCollection(series);
    }

    /**
     * Loads comics from file. Method uses FileReader and Scanner for reading comics from file.
     * <p>
     * The file structure is:
     * number_of_comics (one line with one number, nextInt())
     * comics title (one line with spaces, nextLine())
     * comics author (one line with spaces, nextLine())
     * comics series (one line with spaces, nextLine())
     * cover (one line with one word, next())
     * publish_month (one line with one number, nextInt())
     * publish_year (one line with one number, nextInt())
     * <p>
     * The proper sequence for reading file is to call nextInt(); skip("\n"); to read number of comics.
     * Then in loop call nextLine(); nextLine(); nextLine(), next(); nextInt(); nextInt(); skip("\n").
     * <p>
     * If file does not exists, or is directory, or can not be read, method just ignores it and does nothing.
     *
     * @param file File from which comics will be loaded.
     */
    public void load(File file) {

        // Set<Comic> comics=new HashSet<>();

        if (file.exists()==true) {
            if (file.isDirectory() == false) {
                if(file.canRead()==true){
                    try (FileReader fr = new FileReader(file);
                         Scanner scanner = new Scanner(fr)) {
                        while (scanner.hasNextInt()) {
                            Integer number_of_comics = scanner.nextInt();
                            scanner.skip("\n");

                            for (int i = 1; i <= number_of_comics; i++) {
                                String title = scanner.nextLine();
                                String author = scanner.nextLine();
                                String series = scanner.nextLine();
                                String cover = scanner.next();
                                int publishMonth = scanner.nextInt();
                                scanner.skip("\n");
                                int publishYear = scanner.nextInt();
                                scanner.skip("\n");
                                Comic comic = new Comic(title, author, series, Comic.Cover.valueOf(cover),
                                        publishYear, publishMonth);
//                                comic.setTitle(title);
//                                comic.setAuthor(author);
//                                comic.setSeries(series);
//                                comic.setCover(Comic.Cover.valueOf(cover));
//                                comic.setPublishYear(publishYear);
//                                comic.setPublishMonth(publishMonth);
                                comics.add(comic);
//                                add(comic1);
                            }
                        }
                    } catch (IOException ex) {
                        System.err.println(ex.getMessage());
                        ex.printStackTrace();
                    }

                }
            }
        }
    }

    /**
     * Counts all comics with the same provided series name.
     *
     * @param series Name of the series for which comics will be counted.
     * @return Number of comics from the same provided series.
     */
    public int countBySeries(String series) {
        int counter = 0;
        int comicsConter = 0;

        for (Comic comic : comics) {
            comicsConter++;
            if (comic.getSeries().equals(series)) {
                counter++;
                if (comicsConter == comics.size()) {
                    return counter;
                }

            }


        }


        return counter;
    }

    /**
     * Counts all comics with the same provided author.
     *
     * @param author Author for which comics will be counted.
     * @return Number of comics with the same author.
     */
    public int countByAuthor(String author) {int counter = 0;
        int comicsConter = 0;

        for (Comic comic : comics) {
            comicsConter++;
            if (comic.getAuthor().equals(author)) {
                counter++;
                if (comicsConter == comics.size()) {
                    return counter;
                }

            }


        }


        return counter;
    }

    /**
     * Counts all comics with the same provided publish hear.
     *
     * @param year Publish year for which comics will be counted.
     * @return Number of comics from the same provided publish year.
     */
    public int countByYear(int year) {
        int counter = 0;
        int comicsConter = 0;

        for (Comic comic : comics) {
            comicsConter++;
            if (comic.getPublishYear()== year) {
                counter++;
                if (comicsConter == comics.size()) {
                    return counter;
                }

            }


        }


        return counter;
    }

    /**
     * Counts all comics with the same provided publish hear and month.
     *
     * @param year  Publish year for which comics will be counted.
     * @param month Publish mnt for which comics will be counted.
     * @return Number of comics from the same provided publish year and month.
     */
    public int countByYearAndMonth(int year, int month) {
        int counter = 0;
        int comicsConter = 0;

        for (Comic comic : comics) {
            comicsConter++;
            if (comic.getPublishYear()== year) {
                if(comic.getPublishMonth()==month){
                counter++;
                  if (comicsConter == comics.size()) {
                    return counter;
                  }
                }
            }


        }


        return counter;
    }

    /**
     * Removes all comics with publish year smaller than the provided year. For the removal process
     * method uses iterator.
     *
     * @param year Provided yer.
     */
    public void removeAllOlderThan(int year){

        Iterator <Comic> it = comics.iterator();
        while(it.hasNext()){
            if(it.next().getPublishYear()<year){
                it.remove();
            }
        }
    }

    /**
     * Removes all comics written by the specified author. For the removal process method uses iterator.
     *
     * @param author Provided author.
     */
    public void removeAllFromAuthor(String author) {

        Iterator<Comic> it =comics.iterator();
        while(it.hasNext()){
            if(it.next().getAuthor().equals(author)){
                it.remove();
            }
        }
    }

    /**
     * Creates specified map and returns it.
     *
     * @return Mapping author->comics. Map keys are names of the authors (String) present in the library. Map values are
     * collection (e.g.: HashSet<Comic>) of comics for specified author.
     */
    public Map<String, Collection<Comic>> getAuthorsComics() {
        Map<String,Collection<Comic>>  authorComics=new HashMap<>();


        for(Comic comics:comics){
            if(authorComics.containsKey(comics.getAuthor())!=true){
                Set<Comic> comicCollection= new HashSet<>();
                authorComics.put(comics.getAuthor(), comicCollection);
            }
            authorComics.get(comics.getAuthor()).add(comics);
        }

        return authorComics;
    }

    /**
     * Creates specified map and returns it.
     *
     * @return Mapping publish year->comics. Map keys are publish year (Integer, generics can not be simple types
     * so instead int the Integer is used) present in the library. Map values are collection (e.g.: HashSet<Comic>)
     * of comics for specified author.
     */
    public Map<Integer, Collection<Comic>> getYearsComics() {
        Map<Integer,Collection<Comic>>  yearsComics=new HashMap<>();


        for(Comic comics:comics) {
            if (yearsComics.containsKey(comics.getPublishYear()) != true) {
                Set<Comic> comicCollection = new HashSet<>();
                yearsComics.put(comics.getPublishYear(), comicCollection);
            }
            yearsComics.get(comics.getPublishYear()).add(comics);
        }

        return yearsComics;
    }
    /*
    My implementation to task 4. Remove newer comic than year sent as arguments.
     * @param year Provided yer.
    */

    public void removeAllNewerThan(int year) {
        Iterator<Comic> it = comics.iterator();
        while (it.hasNext()) {
            if (it.next().getPublishYear() >= year) {
                it.remove();
            }
        }


    }

    /**
     * My implementation to task 5.
     * Remove all comic from the set yer and mounth sent via arguments
     *
     * @param year   Provided yer.
     * @param mounth Provided mounth.
     */

    public void removeAllFromYearMonth(int year, int mounth) {
        Iterator<Comic> it = comics.iterator();
        while (it.hasNext()) {
            Comic comic = it.next();
            if (comic.getPublishYear() == year && comic.getPublishMonth() == mounth) {
                it.remove();

            }
        }
//        throw new UnsupportedOperationException();
    }

    /**
     * My implementation to task 6
     */
    public Map<Pair<Integer, Integer>, Collection<Comic>> getYearsMonthsComics() {
        Map<Pair<Integer, Integer>, Collection<Comic>> yearMonth = new HashMap<>();

        Iterator<Comic> it = comics.iterator();
        while (it.hasNext()) {
            Comic comic = it.next();
            Pair<Integer, Integer> pair = Pair.of(comic.getPublishYear(), comic.getPublishMonth());
            if (!(yearMonth.containsKey(pair))) {
                Collection<Comic> comicCollection = new HashSet<>();
                yearMonth.put(pair, comicCollection);
            }
            yearMonth.get(pair).add(comic);

        }
        return yearMonth;

    }


}
